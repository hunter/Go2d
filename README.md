# Go2d

Go2d is a cross-platform 2D game engine written in Javascript. It allows developers to create HTML5 games quickly.

## Platforms

### PC

 - Chrome
 - Safari
 - FireFox
 - IE 9+

### Mobile Browsers / WebView

 - iOS 5.0+
 - Android 2.3+
 - Windows Phone 8(?)

## Documentation

 - API References: http://lanfei.github.io/Go2d/docs/go2d.html
 - Tutorial: *Coming soon*
 - Examples: *Coming soon*

##License

Go2d is available under the terms of the [MIT License](https://github.com/Lanfei/Go2d/blob/master/LICENSE), it is free for anyone to use.

## 简介

Go2d 是一个使用 JavaScript 编写的轻量级跨平台 2D 游戏引擎。开发者可以通过它方便、快速地构建 HTML5 游戏。

## 支持平台

### PC 端

 - Chrome
 - Safari
 - FireFox
 - IE 9+

### 移动端

 - iOS 5.0+
 - Android 2.3+
 - Windows Phone 8（未测试）

## 文档与教程

 - API文档： http://lanfei.github.io/Go2d/docs/go2d.html
 - 入门教程：完善中
 - 示例项目：完善中

## 开源许可

Go2d遵循 [MIT 协议](https://github.com/Lanfei/Go2d/blob/master/LICENSE)，无论个人还是公司，都可以免费自由地使用。
